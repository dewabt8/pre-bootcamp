# E-Corp Inc

Perusahaan ini bergerak pada bidang e-commerce dimana menjual produk kebutuhan-kebutuhan seperti makanan, pakaian dan alat-alat.

Karena semakin banyak user pada aplikasi e-corp dan menambahnya banyak fitur. aplikasi e-corp semakin susah untuk dimaintence dan apabila salah satu layanan mati, maka semua layanan ikut mati karena menggunakkan arsitektur monolith. Disini E-corp menghire saya sebagai devops untuk menyelesaikan problem yang diatas.

# Teknologi sebelumnya

1. Arsitektur Monolith
2. Server On Premise
3. Untuk code repositorynya menggunakkan google drive

Karena saya seorang DevOps maka saya menggunakkan culture devops yand dimana saya menghubungkan antara dev team dengan operations team saling terhubung sehingga pengembangan aplikasi dapat efektif dan efesien

# Kultur DevOps pada Perusahaan

![Image of Flow](image/devops_cycle.png)

## Pada perusahaan yang saya ikuti saya akan memakai pendekatan kultur devops :

    1. Plan :
    Tahap perecanaan seperti requirement aplikasi yang akan di develop

    2. Code
    Melakukan code yang telah direncanakan atau plan

    3. Build
    Build aplikasi dengan melakukan integrasi berbagai code yang telah dilakukan.

    4. Test
    Test aplikasi yang telah dibuild dan rebuild apabila dibutuhkan

    5. Release
    Aplikasi di release ke live environment

    6. Deploy
    Deploy aplikasi ke customer atau user

    7. Operate
    Melakukan operasi code jika dibutuhkan

    8. Monitor
    Monitor aplikasi untuk mengetahui performance aplikasi

Dan flow ini juga biasa disebut CI/CD (Continous Integration /Continous Deployment) dengan melakukan automasi 8 flow diatas. Tugas saya mengotomasi 8 flow tersebut


**Untuk tools yang digunakkan**

1. Micro Service
   <br>![Image of Flow](image/microservice.png)
   <br>Disini saya menggunakkan micro service agar layanan berjalan sendiri-sendiri. keuntungannya menggunakkan arsitektur microservice ketimbang monolith yaitu apabila salah satu layanan ada yang mati maka layanan lain tidak terganggu

2. Jira
   <br>![Image of Flow](image/Jira.png)
   <br>Disini saya menggunakkan jira agar developer dan pm untuk berkolaborasi dengan mudah. Karena tools ini memiliki fitur report dan juga to do task. Sehingga dapat Tools ini dapat memungkinkan dapat mengatur manejemen proyek. 

3. Gitlab
   <br>![Image of Flow](image/gitlab.png)
   <br>Disini saya menggunakkan gitlab untuk sebagai code repository sebelumnya developer menggunakkan gdrive untuk code repositry. Keuntungannya menggunakkan gitlab yaitu dapat mengetahui perubahan code yang sudah dikerjakan.Tools yang digunakkan untuk source repository untuk developer dan Gitlab memiliki fitur CI/CD untuk proses otomasi dari pembuatan code sampai deploy
   
4. Ansible
   <br>![Image of Flow](image/Ansible.png)
   <br>Disini saya menggunakkan ansible untuk membuat server secara berulang2 tanpa manual. 

5. AWS
   <br>![Image of Flow](image/aws.jpg)
   <br>Disini saya memakai Layanan Cloud AWS. Untuk menyewa server secara cloud. keuntungan murah dan sesuai kebutuhan. Karena apabila memakai on-promise biaya yang dibutuhkan sangat mahal

6. Docker
   <br>![Image of Flow](image/docker.png)
   <br>Disini saya menggunakkan Docker. Tool untuk container aplikasi karena memudahkan proses pengembangan software. Dan aplikasi terisolasi sehingga tidak mengganggu service lainnya

7. Kubernetes
   <br>![Image of Flow](image/Kubernetes.png)
   <br>Disini saya menggunakkan Kubernetes. Tool untuk orkestrasi container atau untuk mengelola container container saling bekerja antar satu sama lain

8. Grafana & Promotheus
   <br>![Image of Flow](image/grafana_prometheus.png)
   <br>Disini saya menggunakkan Grafana & promotheus untuk monitoring aplikasi yang berjalan apabila terjadi sesuatu pada aplikasi kita dapat memonitoring atau melihat log pada aplikasi tersebut

9. IP Public & IP Private
   <br>Disini saya menggunakkan IP public untuk mengakses aplikasinya atau web server. Dan saya menggunakkan IP private untuk komunikasi antara web server dengan database server ataupun storage server agar aman dari orang yang tidak bertanggung jawab

10. Domain
   <br>Untuk domain saya menyewa pada pandi.id dengan nama e_corpedia.id agar server saya mudah diingat user saya ketimbang saya hanya menggunakkan ip address

 