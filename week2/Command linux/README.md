# Basic Command Linux

1. Membuat folder baru <br>

        mkdir <nama folder>
    ![Image of Flow](image/1.PNG)

    dan juga beberapa folder <br>
    ![Image of Flow](image/1_2.PNG)

2. Melihat isi content dari suatu directory
   
        ls -l
    ![Image of Flow](image/1_2.PNG)

    Melihat semua sisi termasuk hidden file atau directory
        
        ls -la
    ![Image of Flow](image/2_2.PNG)
    
3. Copy file atau folder
   
        cp <old file> <new file>

    ![Image of Flow](image/3.PNG)

    untuk semua isi gunakkan

        cp -r <old folder> <new folder>
    
    ![Image of Flow](image/3_2.PNG)

4. melihat isi file

        cat <nama file>

    ![Image of Flow](image/3_2.PNG)


5. Membuat file

        touch <nama file>

    ![Image of Flow](image/5.PNG)

6. Menghapus file
   
    option -r digunakkan untuk menghapus yang ada didalamnya

        rm -r <nama folder>

    ![Image of Flow](image/6.PNG)

7. Memindah file atau merubah nama file atau folder
   
        mv <old file> <new file>
    ![Image of Flow](image/7.PNG)

8. untuk masuk directory menggunakkan perintah cd
   
        cd <path directory>
    ![Image of Flow](image/8.PNG)
    
    untuk keluar dari current directory ke parent directory

        cd ..
9.  untuk mencari file

        find <your path> -type <file type> -name <file name>

    ![Image of Flow](image/9.PNG)
10. untuk mencari pattern pada file
    
        grep <nama file>

    ![Image of Flow](image/10.PNG)

11. untuk mengexecute command sebgaai superuser atau user lain
    
        sudo <command execution>
    
    ![Image of Flow](image/11.PNG)

12. Untuk melihat memory, layaknya device manager
    
        htop

    ![Image of Flow](image/12.PNG)

13. Untuk memberikan mode file
    
        sudo chmod <mode> <folder or file>

    ![Image of Flow](image/13.PNG)

14. Untuk membuat user baru
    
        sudo adduser <user name>

    ![Image of Flow](image/14.PNG)


15. Untuk memberikan hak akses sudo

        sudo usermod -aG <group> <user>

    ![Image of Flow](image/15.PNG)


16. Untuk memberikan hak akses user lain gunakkan chown

    ![Image of Flow](image/16.PNG)

        sudo chown <options> <user:group> <file or folder>

    ![Image of Flow](image/16_2.PNG)

17. Untuk menghapus user gunakkan perintah
    
        sudo deluser <user>

    ![Image of Flow](image/17.PNG)


18. Melihat perintah command sebelumnya
    
        history

    ![Image of Flow](image/18.PNG)

    
19. Untuk melakukan compress dan uncompress zip
    
    untuk zip gunakkan perintah 

        zip -r <file.zip> <file to zip>

    untuk unzip gunakkan perintah berikut ini

        unzip -r <file.zip> <file to zip>

    ![Image of Flow](image/19.PNG)

20. untuk mengetahui route-route yang dilewati

        traceroute <domain atau ip address>

    ![Image of Flow](image/20.PNG)


21. Untuk melihat konten suatu web server atau mengambil file
    
        curl <nama server>
    ![Image of Flow](image/21.PNG)

        

    