# Instalasi Ubuntu Server

1. Pertama klik menu new dan berikan nama kemudian klik next
   <br>
   <img src="image/1.PNG" height=300>
   <br>
2. Kemudian pilih berapa ram yang akan dipakai. Disini saya memberikan 4gb of ram. Klik next
   <br>
   <img src="image/2.PNG" height=300>
   <br>
3. Pilih virtual harddisk disini saya membuat virtual harddisk
   <br>
   <img src="image/3.PNG" height=300>
   <br>
   <img src="image/4.PNG" height=300>
   <br>

4. Kemudian pilih dynamically allocated
   <br>
      <img src="image/5.PNG" height=300>
   <br>
6. Kemudian isi size virtual harddisk dan lokasi vhd itu sendiri. Disini saya memakai 20 gb
   <br>
      <img src="image/6.PNG" height=300>
   <br>
7. Ubah settingan network dari nat menjadi bridge adapter
   <br>
   <img src="image/7.PNG" height=300>
   <br>
8. Nyalakan start pada vm yang bernama rizky dan masukkan ubuntu server kemudian klik start
   <br>
   <img src="image/8.PNG" height=300>
   <br>
9.  Pilih languange. Disini saya memilih english
    <br>
   <img src="image/9.PNG" height=300>
   <br>
10. pilih continue without update klik enter
    <br>
    <img src="image/10.PNG" height=300>
   <br>
11. Pilih konfigurasi keyboard. Disini saya memilih English (US)
    <br>
    <img src="image/11.PNG" height=300>
   <br>
12. Pilih network interface pilih yang diatas
    <br>
    <img src="image/12.PNG" height=300>
   <br>
13. Untuk proxy adress kosongkan langsung next
    <br>
    <img src="image/13.PNG" height=300>
   <br>
14. Pilih ubuntu archive mirror disini saya memilih default mirror address
    <br>
    <img src="image/14.PNG" height=300>
   <br>
15. Masukkan profile setup dan juga install open ssh kemudian enter done. Untuk featured snap langsung done
    <br>
    <img src="image/15.PNG" height=300>
   <br>
   <img src="image/16.PNG" height=300>
   <br>
   <img src="image/17.PNG" height=300>
   <br>
15. Proses install tunggu beberapa menit
    <br>
    <img src="image/18.PNG" height=300>
   <br>
16. Instalasi berhasil login dan ketik ifconfig untuk mengetahui ip address dan network adapter
    <br>
    <img src="image/19.PNG" height=300>
   <br>
17. Kemudian untuk merubah ip static ketik command berikut ini
    
        sudo nano /etc/netplan/oo-installer-config.yaml

kemudian isikan seperti berikut ini
<br>
<img src="image/20.PNG" height=300>
<br>

kemudian simpan dan ketik command berikut ini dan juga melihat proses debugnya

        sudo netplan --debug apply
<br>
<img src="image/21.PNG" height=300>
<br>

18. Kemudian kita coba ssh
    <br>
    <img src="image/22.PNG" height=300>
   <br>
