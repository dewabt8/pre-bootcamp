# Version Control System

1. Untuk membuat reposity lokal

        git init .

    ![Image of Flow](image/1.PNG)

2. Untuk melihat satus files pada repositoy lokal
   
        git status

    ![Image of Flow](image/1.PNG)

3. Untuk menghapus file dari repository
   
        git rm --cached -f week1

4. Untuk menambahkan file dari repository
   
        git add .

    ![Image of Flow](image/3.PNG)

5. Untuk membuat commit pada repository

        git commit -m "inital commit"

    ![Image of Flow](image/4.PNG)

6. Untuk merubah branch 
   
        git branch -M <branch name>

    ![Image of Flow](image/5.PNG)

7. untuk menambahkan repository ke gitlab
   

        git remote add <name> <your repository domain>

    apabila ingin merubah

        git remote set-url <name> <your repository domain>

    ![Image of Flow](image/7.PNG)

8. Untuk memasukkan ke gitlab gunakkan
   

        git push origin master
    
    ![Image of Flow](image/8.PNG)


9.  Untuk mengabil repository dari gitlab
    
        git pull origin master

    ![Image of Flow](image/9.PNG)

10. untuk mengclone suatu project
    
        git clone <name repository>

    ![Image of Flow](image/10.PNG)

    