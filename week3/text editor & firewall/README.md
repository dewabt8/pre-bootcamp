# Text Editor with Nano

1. untuk mengecek version pada nano

        nano --version

    <br>
   <img src="image/1.PNG">
   <br>

2. cara membuka text editor pada file
   
        nano <file yang dibuka>
    <br>
   <img src="image/2.PNG">
   <br>

    <br>
   <img src="image/2.PNG" height=300>
   <br>

   shorcut : <br>
   ctrl + x : untuk keluar dari editor
   ctrl + O : untuk melakukan save
   ctrl + w : untuk mencari 
   ctrl + a : untuk memindah cursor berpindah baris awal
   ctrl + e : untuk memindah cursor berpindah baris akhir 
   alt + a atau tab + cursor : untuk memblock suatu kata
   alt + 6 : untuk mengcopy
   ctrl + u : untuk mempaste
   ctrk + k : untuk mengecut suatu kata

<br>

# Text Manipulation

## 1. CAT

Command untuk concate file atau membaca file.
<br>
<br>
Membaca file

    cat file.txt

Untuk membuat file dan memasukkan text (Overide)

    cat > file.txt

    
   <img src="image/3.PNG">
   <br>

Untuk tidak mengoveride 

    cat >> file.txt

   <img src="image/3_2.PNG">
   <br>

Untuk menggabungkan file 

    cat file1 file2 > file3

   <img src="image/3_1.PNG">
   <br>
<br>

## 2. SED
Command untuk stream editor

Untuk menganti kata lama menjadi kata baru

    sed -i 's/<kata lama>/<kata baru>/g' <nama file>

<img src="image/4.PNG">
   <br>


## 3. GREP

Command untuk mencari kata

    grep <kata> file

Untuk menghitung kata pada file

    grep -c <kata> file

untuk mencari kata pada semua file

    grep <kata> *

<img src="image/5.PNG">
   <br>

## 5. SORT

Untuk mengurutkan kata pada file

<br> Ascending
    sort <file>

<br> Descending

    sort -r <file>

<img src="image/6.PNG">
   <br>
<br>

## 6. ECHO

Command untuk print kata

    echo "<Kata>"

    
<img src="image/7.PNG">
   <br>

<br>

## 7. Head & Tail
Head untuk melihat kata di paragraf paling atas<br>
Tail untuk melihat kata di paragraf paling bawah

<img src="image/8.PNG">
   <br>
<br>

# Monitoring

1. htop
   
<img src="image/9.PNG">
   <br>

2. lsof

untuk melihat proses pada suatu user

    lsof -u user

untuk melihat proses pada suatu port

    lsof -i :80

<img src="image/10.PNG">
   <br>

3. ps
   
Untuk melihat semua proses 

    ps -aux

Untuk mehliat proses suatu user

    ps -f -u <user>


<img src="image/11.PNG">
   <br>
<br>

# Firewall

Untuk menampilkan aplikasi yang didukung ufw

    sudo ufw app list

<img src="image/12.PNG">
   <br>
<br>


Untuk menyalakan firewall

    sudo ufw enable

Untuk melihat konfigurasi firewall

    sudo ufw verbose

    sudo ufw allow <nama aplikasi>

Untuk membuka akses suatu port

    sudo ufw allow <port>

    sudo ufw allow <port:udp or tcp>


<img src="image/13.PNG">
   <br>
<br>

<img src="image/14.PNG">
   <br>
<br>

Untuk memblokir port

    sudo ufw deny <port>


<img src="image/15.PNG">
   <br>
<br>

<img src="image/16.PNG">
   <br>
<br>

Untuk menhapus konfigurasi 

    sudo ufw delete deny <aplikasi atau port>