# Application

## Node.Js

1. Pertama install node dengan menggunakkan berikut
        
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash

    <br>
    atau

        wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash


    <img src="image/1.PNG" >
   <br>

2. Kemudian restart bash dan cek versi nvm nya
   

        exec bash
        
        nvm -v

    <img src="image/2.PNG" >
   <br>

3. kemudian install node nya


        nvm install 16

    <img src="image/3.PNG" >
   <br>

    setelah itu cek versi node dan npm

        node -v
        npm -v

    <img src="image/3_1.PNG" >
   <br>
    
4. Kemudian untuk membuat node app
   
        mkdir <nama direktori>
        cd <nama direktori>
        npm init -y

    <img src="image/4.PNG" height=200>
   <br>

   kemudian install framework express.js untuk membuat web sederhana

        npm install express --save

    <img src="image/4_1.PNG" >
   <br>

5. Kemudian buat file index.js
   
        nano index.js

    <img src="image/5.PNG" >
   <br>

    kemudian jalankan dengan menggunakkan perintah

        node index.js

    <img src="image/5_1.PNG" >
   <br>
    <br>
   <img src="image/5_2.PNG" >
   <br>

6. Kemudian untuk menjalankan dibackground kita menggunakkan pm2

        npm install pm2@latest -g

     <img src="image/6.PNG" >
   <br>    

    untuk menyalankan gunakkan perintah

        pm2 start index.js

    untuk memperhentikan gunakkan perintah

        pm2 stop index.js

    untuk restart pm2 gunakkan perintah

        pm2 restart index.js

    <img src="image/6_1.PNG" >
   <br>

   <img src="image/6_2.PNG" >
   <br>    

    <img src="image/5_2.PNG" >
   <br>

## Python

1. Untuk python pada ubuntu sudah tersedia untuk cek sebagai berikut

        python --version

    Kita hanya menginstall package manager python yaitu pip

        sudo apt install -y python3-pip    

    <img src="image/7.PNG" >
   <br>

   kemudian cek versi pip nya

    <img src="image/7_1.PNG" >
   <br>

    Kemudian install virtualenv

        sudo apt install -y python3-virtualenv

    <img src="image/7_2.PNG" >
   <br>

    Kemudian cek  versi virtualenv

        virtualenv --version

2. Kemudian buat python app
   
        mkdir <nama direktori>
        cd <nama direktori>
        virtualenv <nama env>

    <img src="image/8.PNG" >
   <br>  

    Kemudian untuk mengakses virtual env

        . <nama env>/bin/activate

    <img src="image/8_1.PNG" >
   <br>  

    Kemudian install framework flask

        pip3 install Flask

    <img src="image/8_2.PNG" >
   <br>  

    Kemudian buat file index.py

    <img src="image/8_3.PNG" >
   <br> 

   kemudain untuk menjalankkan index.py

        FLASK_APP=index.py flask run

    <img src="image/8_4.PNG" >
   <br> 
    <img src="image/9_1.PNG" >
   <br> 

3. untuk menjalankan python dibackground bisa gunakkan pm2 karena pm2 sudah dijelaskan seperti diatas. maka langkah selanjutnya

        pm2 start "FLASK_APP=index.py flask run"

    <img src="image/9.PNG" >
   <br>

   <img src="image/9_1.PNG" >
   <br> 
    <br>

## GO

1. Untuk install go
   
        wget https://golang.org/dl/go1.17.2.linux-amd64.tar.gz && sudo su

    <img src="image/10.PNG" >
   <br> 

2. Kemudian extract dan copy

        rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.2.linux-amd64.tar.gz && exit

    <img src="image/11.PNG" >
   <br> 
    
3. Kemudian tambahkan path nya pada bashrc

        nano ~/.bashrc

    <img src="image/12.PNG" >
   <br> 
    
    kemudian restart bashnya

        exec bash

    <img src="image/12_1.PNG" >
   <br> 

4. Untuk membuat go app

        mkdir go-app
        cd go-app
        nano index.go

    <img src="image/13.PNG" >
   <br> 

   untuk menjalankan bisa dengan dua cara

          go run index.go

   atau dicompile dulu

        go build index.go
        ./index

    <img src="image/13_1.PNG" >
   <br> 

5. Kemudian untuk menjalankan dibackground
   pertama compile dulu

        go build index.go

    kemudian 

        nohup ./index &

    <img src="image/13_2.PNG" >
   <br> 
    
    <img src="image/13_1.PNG" >
   <br> 
