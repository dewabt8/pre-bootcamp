# Webserver & Load Balancing

# webserver

1. Pertama install nginx
   
        sudo apt install -y nginx

    kemudian cek versi

        nginx -v

2. Kemudian untuk menjalankan nginx
   
        sudo systemctl start nginx

    kemudian cek status nginx

        sudo systemctl status nginx

    <img src="image/1.PNG" >
   <br> 

    <img src="image/1_1.PNG" >
   <br> 

## Menjalankan application on web server

1. Node App
   
   - Pertama nyalakan pm2 start index.js
    <br>
    <br>
    <img src="image/2.PNG" >
   <br> 
   - Kemudian buat direktori di /etc/nginx
    <br>

            sudo mkdir app_domain
            cd app_domain

     dan buat konfigurasi

            sudo nano node.rizky.conf

     <br>
     <img src="image/2_1.PNG" >
    <br>    
   <br>
    
   - kemudian tambahkann di /etc/nginx.conf agar dienable
     <br>
    <img src="image/2_2.PNG" >
   <br>
   - Kemudian cek konfigurasi apakah success

            sudo nginx -t

        <br>
        <img src="image/2_3.PNG" >
        <br>

   - Kemudian kita reload nginx


            sudo systemctl reload nginx

   - Karena kita lokal pengujiannya tambahkan di /etc/hosts seakan2 mempunyai domain

        <br>
        <img src="image/2_4.PNG" >
        <br>
   - Hasilnya seperti berikut
                <br>
        <img src="image/2_5.PNG" >
        <br>

   
2. Python App
   <br>
   - Untuk python app kita nyalakan dengan menggunakkan pm2

            . my-env/bin/activate
    <br>

            pm2 start "FLASK_APP=main.py flask run"
    <br>
    <img src="image/3.PNG" >
    <br>
    tambahkan config di node.rizky.conf
    
    <img src="image/3_1.PNG" >
    <br>

   - kemudian cek dengan sudo nginx -t
   - setelah itu reload nginxnya
   - dan tambah sub domain ke etc hosts
   <img src="image/3_2.PNG" >
    <br>
   - Hasilnya seperti berikut
  <img src="image/3_3.PNG" >
    <br>

3. GO App
   - Untuk GO app untuk menyalakanna dengan mengunakkan nohup ./index
    <br>
    <img src="image/4.PNG" >
    <br>
   - kemudian tambah config di node.rizky.conf
        <br>
    <img src="image/4_1.PNG" >
    <br>
   - lakukan hal sama seperti no 1 dan 2
    <br>
         
            sudo nginx -t
            sudo systemctl reload nginx
            sudo nano /etc/hosts

   - hasilnya seperti berikut ini
    <br>
    <img src="image/4_2.PNG" >
    <br>
  

## Load Balancing

1. Disini kita buat configurasinya seperti ini
   <br>
    <img src="image/5.PNG" >
    <br>
2. Kemudian kita lakukan cek konfigurasinya sudo    nginx -t
   
        sudo nginx -t
        sudo systemctl reload nginx

3. Dan hasilnya seperti berikut
   
   <br>
    <img src="image/5_1.PNG" >
    <br>
    <br>
    <img src="image/5_2.PNG" >
    <br>
    <br>
    <img src="image/5_3.PNG" >
    <br>
